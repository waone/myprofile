<?php
function getBaseUrl()
{
	// output: /myproject/index.php
	$currentPath = $_SERVER['PHP_SELF'];

	// output: Array ( [dirname] => /myproject [basename] => index.php [extension] => php [filename] => index )
	$pathInfo = pathinfo($currentPath);

	// output: localhost
	$hostName = $_SERVER['HTTP_HOST'];

	// output: http://
	$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https'?'https':'http';

	// return: http://localhost/myproject/
	return $protocol.'://'.$hostName.$pathInfo['dirname']."/";
}
?>

<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<!-- <link rel="shortcut icon" href="img/fav.png"> -->
	<!-- Author Meta -->
	<meta name="author" content="colorlib">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title>Publication</title>

	<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700,900" rel="stylesheet">
	<!--
			CSS
			============================================= -->
	<link rel="stylesheet" href="css/linearicons.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/magnific-popup.css">
	<link rel="stylesheet" href="css/nice-select.css">
	<link rel="stylesheet" href="css/animate.min.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/main.css">
</head>

<body>

	<!-- Start Header Area -->
	<header id="header">
		<div class="container main-menu">
			<div class="row align-items-center d-flex">
				<nav id="nav-menu-container" style="margin-left: 0px ! important;">
					<ul class="nav-menu">
						<li class=""><a class="active" href="<?= getBaseUrl() ?>">Home</a></li>
						<li class="menu-has-children"><a href="#">Repo</a>
							<ul>
								<li><a href="https://gitlab.com/waone">Codeigniter</a></li>
								<li><a href="https://gitlab.com/waone">Laravel</a></li>
								<li><a href="https://github.com/waonehenry/machine-learning">Python</a></li>
							</ul>
						</li>
						<li><a href="<?= getBaseUrl() ?>downloads">Downloads</a></li>
						<li class=""><a class="" href="<?= getBaseUrl() ?>publication.php">Publication</a></li>
						<li class=""><a class="" href="<?= getBaseUrl() ?>portofolio.php">Creation</a></li>
					</ul>
				</nav>
			</div>
		</div>
	</header>
	<!-- End Header Area -->

	<!-- start banner Area -->
	<section class="banner-area relative">
		<div class="container">
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">
						Publication
					</h1>
					<p class="link-nav">
						<span class="box">
							<a href="<?= getBaseUrl() ?>">Home </a>
							<a class="" href="<?= getBaseUrl() ?>publication.php">Publication</a>
						</span>
					</p>
				</div>
			</div>
		</div>
	</section>
	<!-- End banner Area -->

	<!-- Start Align Area -->
	<div class="whole-wrap">
		<div class="container">
			<div class="section-top-border">
				<h3 class="mb-30">Table of Content</h3>
				<!-- <div class="row">
					<div class="col-lg-12">
						<blockquote class="generic-blockquote">
							“We are controlling technology”
						</blockquote>
					</div>
				</div> -->
				<div class="progress-table-wrap">
					<div class="progress-table">
						<div class="table-head">
							<div class="serial">#</div>
							<div class="country">Title</div>
							<div class="visit">Published</div>
							<div class="percentage">Download</div>
						</div>
						<div class="table-row">
							<div class="serial">01</div>
							<div class="country">
								PREDIKSI INFEKSI SALURAN PERNAFASAN AKUT (ISPA) DENGAN MENGGUNAKAN METODE RANTAI MARKOV PADA KLINIK CIHIDEUNG
							</div>
							<div class="visit">Seminar Nasional Teknologi Informasi dan Multimedia 2015</div>
							<div class="percentage">
								Unavailable
							</div>
						</div>
						<div class="table-row">
							<div class="serial">02</div>
							<div class="country">
									Integrasi Sistem Informasi: Akses Informasi Sumber Daya Fasilitas Kesehatan dalam Pelayanan Rujukan
							</div>
							<div class="visit">Jurnal Sisfo Vol. 06 No. 01 (2016) 51–64</div>
							<div class="percentage">
								Unavailable
							</div>
						</div>
						<div class="table-row">
							<div class="serial">03</div>
							<div class="country">
									STRATEGI IMPLEMENTASI SISTEM ELOGISTIK SECARA NASIONAL: UPAYA PEMANTAUAN KETERSEDIAAN OBAT DI INDONESIA
							</div>
							<div class="visit">FORUM INFORMATIKA KESEHATAN INDONESIA 2017</div>
							<div class="percentage">
								Unavailable
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="section-top-border">
				<h3 class="mb-30">Participant of</h3>
				<div class="progress-table-wrap">
					<div class="progress-table">
						<div class="table-head">
							<div class="serial">#</div>
							<div class="country">Agenda</div>
							<div class="visit">Event Organizer</div>
							<div class="percentage">Time</div>
						</div>
						<div class="table-row">
							<div class="serial">01</div>
							<div class="country">
								DHIS2 Level 2 Server Administration
							</div>
							<div class="visit"></div>
							<div class="percentage">
								Kerala, India, 2018
							</div>
						</div>
						<div class="table-row">
							<div class="serial">02</div>
							<div class="country">
									Enabling Interoperability with FHIR: The AeHIN’s Regional FHIR Community Workshop
							</div>
							<div class="visit">AEHIN, ASEAN Word Bank</div>
							<div class="percentage">
								Manila, Philipine, 2020
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Align Area -->

	<!-- Horizontal bar -->
	<div class="container">
		<hr>
	</div>

	<!-- start footer Area -->
	<footer class="footer-area" style="padding: 25px 0px ! important;">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-12">
					<div class="footer-top flex-column">
						<div class="footer-logo">
							<!-- <a href="#">
								<img src="img/logo.png" alt="">
							</a> -->
							<h4>Follow Me</h4>
						</div>
						<div class="footer-social">
							<a href="#"><i class="fa fa-whatsapp"></i></a>
							<a href="#"><i class="fa fa-twitter"></i></a>
							<a href="#"><i class="fa fa-instagram"></i></a>
							<a href="#"><i class="fa fa-facebook"></i></a>
						</div>
					</div>
				</div>
			</div>
			<div class="row footer-bottom justify-content-center">
				<p class="col-lg-8 col-sm-12 footer-text">
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
			</div>
		</div>
	</footer>
	<style type="text/css">
	.serial {
			width: 10% ! important;
	}
	.country {
			width: 40% ! important;
	}
	.visit {
			width: 40% ! important;
	}
	.percentage {
			width: 10% ! important;
	}
	</style>

	<script src="js/vendor/jquery-2.2.4.min.js "></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js " integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q "
	 crossorigin="anonymous "></script>
	<script src="js/vendor/bootstrap.min.js "></script>
	<script src="js/easing.min.js "></script>
	<script src="js/hoverIntent.js "></script>
	<script src="js/superfish.min.js "></script>
	<script src="js/mn-accordion.js "></script>
	<script src="js/jquery.ajaxchimp.min.js "></script>
	<script src="js/jquery.magnific-popup.min.js "></script>
	<script src="js/owl.carousel.min.js "></script>
	<script src="js/jquery.nice-select.min.js "></script>
	<script src="js/isotope.pkgd.min.js "></script>
	<script src="js/jquery.circlechart.js "></script>
	<script src="js/mail-script.js "></script>
	<script src="js/wow.min.js "></script>
	<script src="js/main.js "></script>
</body>

</html>
