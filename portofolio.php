<?php
function getBaseUrl()
{
	// output: /myproject/index.php
	$currentPath = $_SERVER['PHP_SELF'];

	// output: Array ( [dirname] => /myproject [basename] => index.php [extension] => php [filename] => index )
	$pathInfo = pathinfo($currentPath);

	// output: localhost
	$hostName = $_SERVER['HTTP_HOST'];

	// output: http://
	$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https'?'https':'http';

	// return: http://localhost/myproject/
	return $protocol.'://'.$hostName.$pathInfo['dirname']."/";
}
?>
<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<!-- <link rel="shortcut icon" href="img/fav.png"> -->
	<!-- Author Meta -->
	<meta name="author" content="colorlib">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title>Creations</title>

	<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700,900" rel="stylesheet">
	<!--
			CSS
			============================================= -->
	<link rel="stylesheet" href="css/linearicons.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/magnific-popup.css">
	<link rel="stylesheet" href="css/nice-select.css">
	<link rel="stylesheet" href="css/animate.min.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/main.css">
</head>

<body>

	<!-- Start Header Area -->
	<header id="header">
		<div class="container main-menu">
			<div class="row align-items-center d-flex">
				<nav id="nav-menu-container" style="margin-left: 0px ! important;">
					<ul class="nav-menu">
						<li class=""><a class="active" href="<?= getBaseUrl() ?>">Home</a></li>
						<li class="menu-has-children"><a href="#">Repo</a>
							<ul>
								<li><a href="https://gitlab.com/waone">Codeigniter</a></li>
								<li><a href="https://gitlab.com/waone">Laravel</a></li>
								<li><a href="https://github.com/waonehenry/machine-learning">Python</a></li>
							</ul>
						</li>
						<li><a href="<?= getBaseUrl() ?>downloads">Downloads</a></li>
						<li class=""><a class="" href="<?= getBaseUrl() ?>publication.php">Publication</a></li>
						<li class=""><a class="" href="<?= getBaseUrl() ?>portofolio.php">Creation</a></li>
					</ul>
				</nav>
			</div>
		</div>
	</header>
	<!-- End Header Area -->

	<!-- start banner Area -->
	<section class="banner-area relative">
		<div class="container">
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">
						Creations
					</h1>
					<p class="link-nav">
						<span class="box">
							<a href="index.html">Home </a>
							<a href="elements.html">Creations</a>
						</span>
					</p>
				</div>
			</div>
		</div>
	</section>
	<!-- End banner Area -->

	<!-- Start Align Area -->
	<div class="whole-wrap">
		<div class="container">
			<div class="section-top-border">
				<h3 class="mb-30">SIMRS</h3>
				<div class="row">
					<div class="col-md-3">
						<img src="img/elements/d.jpg" alt="" class="img-fluid">
					</div>
					<div class="col-md-9 mt-sm-20 left-align-p">
						<p>Recently, the US Federal government banned online casinos from operating in America by making it illegal to transfer
							money to them through any US bank or payment system. As a result of this law, most of the popular online casino networks
							such as Party Gaming and PlayTech left the United States. Overnight, online casino players found themselves being
							chased by the Federal government. But, after a fortnight, the online casino industry came up with a solution and new
							online casinos started taking root. These began to operate under a different business umbrella, and by doing that,
							rendered the transfer of money to and from them legal. A major part of this was enlisting electronic banking systems
							that would accept this new clarification and start doing business with me. Listed in this article are the electronic
							banking systems that accept players from the United States that wish to play in online casinos.</p>
					</div>
				</div>
			</div>
			<div class="section-top-border text-right">
				<h3 class="mb-30">SIMKLAIM</h3>
				<div class="row">
					<div class="col-md-9">
						<p class="text-right">Over time, even the most sophisticated, memory packed computer can begin to run slow if we don’t
							do something to prevent it. The reason why has less to do with how computers are made and how they age and more to
							do with the way we use them. You see, all of the daily tasks that we do on our PC from running programs to downloading
							and deleting software can make our computer sluggish. To keep this from happening, you need to understand the reasons
							why your PC is getting slower and do something to keep your PC running at its best. You can do this through regular
							maintenance and PC performance optimization programs</p>
						<p class="text-right">Before we discuss all of the things that could be affecting your PC’s performance, let’s talk
							a little about what symptoms</p>
					</div>
					<div class="col-md-3">
						<img src="img/elements/d.jpg" alt="" class="img-fluid">
					</div>
				</div>
			</div>
			<div class="section-top-border">
				<h3 class="mb-30">OTHERS</h3>
				<div class="row">
					<div class="col-md-4">
						<div class="single-defination">
							<h4 class="mb-20">E-logistics</h4>
							<p>Recently, the US Federal government banned online casinos from operating in America by making it illegal to transfer
								money to them through any US bank or payment system. As a result of this law, most of the popular online casino networks</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="single-defination">
							<h4 class="mb-20">Nusacare</h4>
							<p>Recently, the US Federal government banned online casinos from operating in America by making it illegal to transfer
								money to them through any US bank or payment system. As a result of this law, most of the popular online casino networks</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="single-defination">
							<h4 class="mb-20">Troy-WMS</h4>
							<p>Recently, the US Federal government banned online casinos from operating in America by making it illegal to transfer
								money to them through any US bank or payment system. As a result of this law, most of the popular online casino networks</p>
						</div>
					</div>
				</div>
			</div>
			<div class="section-top-border">
				<h3 class="mb-30">Block Quotes</h3>
				<div class="row">
					<div class="col-lg-12">
						<blockquote class="generic-blockquote">
							“Recently, the US Federal government banned online casinos from operating in America by making it illegal to transfer money
							to them through any US bank or payment system. As a result of this law, most of the popular online casino networks
							such as Party Gaming and PlayTech left the United States. Overnight, online casino players found themselves being
							chased by the Federal government. But, after a fortnight, the online casino industry came up with a solution and new
							online casinos started taking root. These began to operate under a different business umbrella, and by doing that,
							rendered the transfer of money to and from them legal. A major part of this was enlisting electronic banking systems
							that would accept this new clarification and start doing business with me. Listed in this article are the electronic
							banking”
						</blockquote>
					</div>
				</div>
			</div>
			<div class="section-top-border">
				<h3>Image Gallery</h3>
				<div class="row gallery-item">
					<div class="col-md-4">
						<a href="img/elements/g1.jpg" class="img-gal"><div class="single-gallery-image" style="background: url(img/elements/g1.jpg);"></div></a>
					</div>
					<div class="col-md-4">
						<a href="img/elements/g2.jpg" class="img-gal"><div class="single-gallery-image" style="background: url(img/elements/g2.jpg);"></div></a>
					</div>
					<div class="col-md-4">
						<a href="img/elements/g3.jpg" class="img-gal"><div class="single-gallery-image" style="background: url(img/elements/g3.jpg);"></div></a>
					</div>
					<div class="col-md-6">
						<a href="img/elements/g4.jpg" class="img-gal"><div class="single-gallery-image" style="background: url(img/elements/g4.jpg);"></div></a>
					</div>
					<div class="col-md-6">
						<a href="img/elements/g5.jpg" class="img-gal"><div class="single-gallery-image" style="background: url(img/elements/g5.jpg);"></div></a>
					</div>
					<div class="col-md-4">
						<a href="img/elements/g6.jpg" class="img-gal"><div class="single-gallery-image" style="background: url(img/elements/g6.jpg);"></div></a>
					</div>
					<div class="col-md-4">
						<a href="img/elements/g7.jpg" class="img-gal"><div class="single-gallery-image" style="background: url(img/elements/g7.jpg);"></div></a>
					</div>
					<div class="col-md-4">
						<a href="img/elements/g8.jpg" class="img-gal"><div class="single-gallery-image" style="background: url(img/elements/g8.jpg);"></div></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Align Area -->

	<!-- Horizontal bar -->
	<div class="container">
		<hr>
	</div>

	<!-- start footer Area -->
	<footer class="footer-area ">
		<div class="container ">
			<div class="row justify-content-center ">
				<div class="col-lg-12 ">
					<div class="footer-top flex-column ">
						<div class="footer-logo ">
							<a href="# ">
								<img src="img/logo.png " alt=" ">
							</a>
							<h4>Follow Me</h4>
						</div>
						<div class="footer-social ">
							<a href="# "><i class="fa fa-facebook "></i></a>
							<a href="# "><i class="fa fa-twitter "></i></a>
							<a href="# "><i class="fa fa-dribbble "></i></a>
							<a href="# "><i class="fa fa-behance "></i></a>
						</div>
					</div>
				</div>
			</div>
			<div class="row footer-bottom justify-content-center ">
				<p class="col-lg-8 col-sm-12 footer-text ">
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
			</div>
		</div>
	</footer>
	<!-- End footer Area -->

	<script src="js/vendor/jquery-2.2.4.min.js "></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js " integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q "
	 crossorigin="anonymous "></script>
	<script src="js/vendor/bootstrap.min.js "></script>
	<script src="js/easing.min.js "></script>
	<script src="js/hoverIntent.js "></script>
	<script src="js/superfish.min.js "></script>
	<script src="js/mn-accordion.js "></script>
	<script src="js/jquery.ajaxchimp.min.js "></script>
	<script src="js/jquery.magnific-popup.min.js "></script>
	<script src="js/owl.carousel.min.js "></script>
	<script src="js/jquery.nice-select.min.js "></script>
	<script src="js/isotope.pkgd.min.js "></script>
	<script src="js/jquery.circlechart.js "></script>
	<script src="js/mail-script.js "></script>
	<script src="js/wow.min.js "></script>
	<script src="js/main.js "></script>
</body>

</html>
